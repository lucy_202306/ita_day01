# ORID

## O

​	In the morning, we first carried out ice-breaking activities, then understood the composition structure of the learning pyramid, and finally introduced PDCA: P is plan, D is do, C is check, A is adjust, and practiced PDCA through a demo. In the afternoon, we mainly studied Concept Map and understood its components, including: Focus Question, Concepts, Linking words and Proposition, know the method of building Concept Map, and deepen the learning of Concept Map through group discussion on software. Finally, we introduced and learned the meaning of Standup Meeting and ORID

## R

I was fruitful

## I

Why learn Concept Map? Because it can help us to break down the key issues into multiple topics and explain the interrelationships between topics, so that we can better consolidate knowledge and combine new knowledge with existing knowledge.

## D

Visualize abstract thinking, and use Concept Map to brainstorm appropriately in future team cooperation.